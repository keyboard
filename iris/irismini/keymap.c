#include QMK_KEYBOARD_H

extern keymap_config_t keymap_config;

/* Layer numbers */
#define _QWERTY 0
#define _LOWER 1
#define _RAISE 2
#define _ADJUST 3

/* Key short cuts */
#define ooo KC_TRNS
#define xxx KC_NO

/* Left mod cluster */
#define SFTZ SFT_T(KC_Z)
#define CTRLX CTL_T(KC_X)
#define ALTC ALT_T(KC_C)

/* Right mod cluster */
#define ALTCOMM ALT_T(KC_COMM)
#define CTRLDOT CTL_T(KC_DOT)
#define SFTSLSH SFT_T(KC_SLSH)

/* Left thumb cluster  */
#define MOD2 LGUI_T(KC_ENT)
#define MOD3 LT(_LOWER, KC_SPC)

/* Right thumb cluster  */
#define MOD4 LT(_RAISE, KC_DEL)
#define MOD5 LGUI_T(KC_ESC)

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_QWERTY] = LAYOUT( \
   xxx,   xxx,     xxx,     xxx,     xxx,     xxx,                          xxx,     xxx,     xxx,     xxx,        xxx,   xxx,
   xxx,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                         KC_Y,    KC_U,    KC_I,    KC_O,       KC_P,   xxx,
   xxx,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                         KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,   xxx,
   xxx,  SFTZ,   CTRLX,    ALTC,    KC_V,    KC_B,     xxx,       xxx,     KC_N,    KC_M, ALTCOMM, CTRLDOT,    SFTSLSH,   xxx,
                                  KC_TAB,    MOD2,    MOD3,       MOD4,    MOD5,  KC_BSPC
),
[_RAISE] = LAYOUT( \
   xxx,     xxx,     xxx,      xxx,      xxx,         xxx,                          xxx,      xxx,     xxx,     xxx,        xxx,   xxx,
   xxx, KC_EXLM,   KC_AT,  KC_HASH,   KC_DLR,     KC_PERC,                      KC_PLUS,     KC_7,    KC_8,    KC_9,       KC_0,   xxx,
   xxx, KC_CIRC,  KC_AMPR,  KC_TILD, KC_PIPE,     KC_BSLS,                      KC_MINS,     KC_4,    KC_5,    KC_6,     KC_EQL,   xxx,
   xxx,  KC_DQT,  KC_QUOT,  KC_UNDS,  KC_GRV,         ooo, xxx,           xxx,  KC_ASTR,     KC_1,    KC_2,    KC_3,        ooo,   xxx,
                                         ooo, MO(_ADJUST), ooo,           ooo,      ooo,      ooo
),
[_LOWER] = LAYOUT( \
   xxx,     xxx,     xxx,       xxx,    xxx,      xxx,                    xxx,     xxx,     xxx,     xxx,      xxx, xxx,
   ooo,  KC_INS, KC_HOME,   KC_PGUP, KC_LPRN, KC_RPRN,                KC_MPRV, KC_VOLD, KC_VOLU,  KC_MNXT, KC_MPLY, ooo,
   ooo,  KC_DEL,  KC_END, KC_PGDOWN, KC_LBRC, KC_RBRC,                KC_LEFT, KC_DOWN,   KC_UP, KC_RIGHT, KC_WH_U, ooo,
   ooo, KC_BTN1, KC_BTN2,   KC_PSCR, KC_LCBR, KC_RCBR, ooo,  ooo,     KC_MS_L, KC_MS_D, KC_MS_U,  KC_MS_R, KC_WH_D, ooo,
                                         ooo,     ooo, ooo,  ooo, MO(_ADJUST),     ooo
),

[_ADJUST] = LAYOUT( \
   xxx,    xxx,   xxx,   xxx,   xxx,   xxx,            xxx,   xxx,   xxx,   xxx,    xxx, xxx,
   ooo,  KC_F1, KC_F2, KC_F3, KC_F4, KC_F5,          KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, ooo,
   ooo, KC_F11,   ooo,   ooo,   ooo,   ooo,            ooo,   ooo,   ooo,   ooo, KC_F12, ooo,
   ooo,    ooo,   ooo,   ooo,   ooo,   ooo, ooo,  ooo, ooo,   ooo,   ooo,   ooo,    ooo, ooo,
                                ooo,   ooo, ooo,  ooo, ooo,   ooo
)
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_QWERTY);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
  }
  return true;
}
